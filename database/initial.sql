CREATE DATABASE crud_new_tics;

USE crud_new_tics;

CREATE TABLE `user` (
    `cod_user` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `last_name` VARCHAR(50) NOT NULL,
    `phone` VARCHAR(25) NOT NULL,
    `email` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`cod_user`)
) ENGINE = InnoDB;

create table `skills`
(
	cod_skill INT NOT NULL AUTO_INCREMENT,
	cod_user int not null,
	data longtext not null,
	constraint skills_pk
		primary key (cod_skill),
	constraint skills_user_cod_user_fk
		foreign key (cod_user) references user (cod_user)
);

create table `laboral_reference`
(
	cod_laboral_reference INT NOT NULL AUTO_INCREMENT,
	cod_user int not null,
	data longtext not null,
	constraint laboral_reference_pk
		primary key (cod_laboral_reference),
	constraint laboral_reference_user_cod_user_fk
		foreign key (cod_user) references user (cod_user)
);

create table `user_studies`
(
	cod_user_studies INT NOT NULL AUTO_INCREMENT,
	cod_user int not null,
	data longtext not null,
	constraint user_studies_pk
		primary key (cod_user_studies),
	constraint user_studies_user_cod_user_fk
		foreign key (cod_user) references user (cod_user)
);

create table `further_studie`
(
	cod_futher_studie INT NOT NULL AUTO_INCREMENT,
	cod_user int not null,
	data longtext not null,
	constraint ` further_studie_pk`
		primary key (cod_futher_studie),
	constraint ` further_studie_user_cod_user_fk`
		foreign key (cod_user) references user (cod_user)
);

create table `mastered_languages`
(
	`cod_ mastered_languages` INT NOT NULL AUTO_INCREMENT,
	cod_user int not null,
	data longtext not null,
	constraint ` mastered_languages_pk`
		primary key (`cod_ mastered_languages`),
	constraint ` mastered_languages_user_cod_user_fk`
		foreign key (cod_user) references user (cod_user)
);

create table `reference`
(
	cod_reference INT NOT NULL AUTO_INCREMENT,
	cod_user int not null,
	data longtext not null,
	constraint reference_pk
		primary key (cod_reference),
	constraint reference_user_cod_user_fk
		foreign key (cod_user) references user (cod_user)
);

INSERT INTO `user` (`cod_user`, `email`, `last_name`, `name`, `phone`) VALUES
(1, 'sebastian.masmela@pysltda.com', 'Masmela', 'sebastian', '3196410243');

INSERT INTO `skills` (`cod_skill`, `cod_user`, `data`) VALUES
(1, 1, '[\"Analisis de datos\", \"Benckmarking\", \"Relacion dcon proveedores\", \"Trabajo bajo presion y proactiva\", {\"Programas manejados:\": \"Excel, PowerPoint, Word, Elisa.\"}, {\"Nociones en:\": \"Photoshop, Indesing.\"}]');

INSERT INTO `laboral_reference` (`cod_laboral_reference`, `cod_user`, `data`) VALUES
(1, 1, '[{\"date\": [\"00-00-0000\", \"00-00-0000\", \"[Ciudad, pais]\"], \"enterprise\": \"NOMBRE DE LA EMPRESA\", \"place\": \"CARGO OCUPADO\", \"tasks\": \"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\"}]');

INSERT INTO `user_studies` (`cod_user_studies`, `cod_user`, `data`) VALUES
(1, 1, '[{\"year\": \"AÑO\", \"city\": \"CIUDAD\", \"country\": \"PAIS\", \"Titulo:\": \"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\", \"university\": \"Universidad o institucion\"}]\r\n');

INSERT INTO `further_studie` (`cod_futher_studie`, `cod_user`, `data`) VALUES
(1, 1, '[\"Diplomados, Congresos, Concursos, Proyectos Humanitarios, xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\"]');

INSERT INTO `mastered_languages` (`cod_ mastered_languages`, `cod_user`, `data`) VALUES
(1, 1, '{\"Español\": \"Natal\", \"Ingles\": \"Acanzado\", \"Xxxxxxxx\": \"xxxxx\"}');

INSERT INTO `reference` (`cod_reference`, `cod_user`, `data`) VALUES
(1, 1, '[{\"Nombre\": \"Apellido\"}, \"Empresa\", \"(Ciudad-Pais)\", \"Cargo\", {\"Telefono\": \"xxxxxxxxx\"}, {\"E-mail\": \"xxxxxxxxxx@xxxxx.com\"}]');