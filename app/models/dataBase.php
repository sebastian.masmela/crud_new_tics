<?php

class dataBase
{
    private $conexion, $host, $user, $password, $data_base;

    /**
     * @param string $sql
     * @return array|false|string
     */
    public function searchDB(string $sql)
    {
        self::createConexion();
        try {
            $result = mysqli_query($this->conexion, $sql);
        } catch (Exception $e) {
            echo '{"Error": "Review SQL Script"}';
            die;
        }
        if ($result) {
            $response = [];
            while ($row = mysqli_fetch_assoc($result)) {
                $response[] = $row;
            }
            self::closeConexion();
            return $response;
        } else {
            self::closeConexion();
            echo '{"Error": "The query could not be made in the database"}' . PHP_EOL;
            die;
        }
    }

    /**
     * @param string $sql
     * @return int|string
     */
    public function insertDB(string $sql)
    {
        self::createConexion();
        $result = mysqli_query($this->conexion, $sql);
        if ($result) {
            $last_id = mysqli_insert_id($this->conexion);
            self::closeConexion();
            return $last_id;
        } else {
            self::closeConexion();
            echo '{"Error": "The query could not be made in the database"}' . PHP_EOL;
            die;
        }
    }

    /**
     * @param string $sql
     * @return bool
     */
    public function executeDB(string $sql)
    {
        self::createConexion();
        $result = mysqli_query($this->conexion, $sql);
        if (!$result) {
            self::closeConexion();
            echo '{"Error": "The query could not be made in the database"}' . PHP_EOL;
            die;
        }
        return true;
    }

    /**
     * @desc
     * @return void
     */
    private function createConexion(): void
    {
        $this->host = 'localhost';
        $this->user = 'root';
        $this->password = '';
        $this->data_base = 'crud_new_tics';
        $this->conexion = mysqli_connect(
            $this->host,
            $this->user,
            $this->password,
            $this->data_base
        );
        if (!$this->conexion) {
            echo '{"Error": "Unable to connect to the database", "Debugging error": "' . mysqli_connect_errno() . '"}' . PHP_EOL;
            die;
        }
    }

    /**
     * @return void
     */
    private function closeConexion(): void
    {
        mysqli_close($this->conexion);
    }
}
