<?php
require_once 'C:\xampp\htdocs\crud_new_tics\app\models\dataBase.php';
$dataBase = new dataBase();

if (!empty($_GET)) {
    if (isset($_GET['funcion'])) {
        $function = $_GET['funcion'];
        switch ($function) {
            case 'obtenerUsuario':
                $cod_usuario = $_GET['cod_usuario'];
                $user_sql = 'SELECT cod_user,
                                    name,
                                    last_name,
                                    phone,
                                    email
                             FROM crud_new_tics.user
                             WHERE cod_user = ' . $cod_usuario;
                $usuario = $dataBase->searchDB($user_sql)[0];
                $skills_sql = 'SELECT data
                               FROM crud_new_tics.skills
                               WHERE cod_user = ' . $cod_usuario;
                $data_skills = $dataBase->searchDB($skills_sql)[0];
                $skills = json_decode($data_skills['data'], true);
                $laboral_references_sql = 'SELECT data
                                           FROM crud_new_tics.laboral_reference
                                           WHERE cod_user = ' . $cod_usuario;
                $data_laboral_references = $dataBase->searchDB($laboral_references_sql)[0];
                $laboral_references = json_decode($data_laboral_references['data'], true);
                $user_studies_sql = 'SELECT data
                                     FROM crud_new_tics.user_studies
                                     WHERE cod_user = ' . $cod_usuario;
                $data_user_studies = $dataBase->searchDB($user_studies_sql)[0];
                $user_studies = json_decode($data_user_studies['data'], true);
                $mastered_languages_sql = 'SELECT data
                                           FROM crud_new_tics.mastered_languages
                                           WHERE cod_user = ' . $cod_usuario;
                $data_mastered_language = $dataBase->searchDB($mastered_languages_sql)[0];
                $mastered_languages = json_decode($data_mastered_language['data'], true);
                $futher_studie_sql = 'SELECT data
                                      FROM crud_new_tics.further_studie
                                      WHERE cod_user = ' . $cod_usuario;
                $data_further_studie = $dataBase->searchDB($futher_studie_sql)[0];
                $further_studie = json_decode($data_further_studie['data'], true);
                $reference_sql = 'SELECT data
                                      FROM crud_new_tics.reference
                                      WHERE cod_user = ' . $cod_usuario;
                $data_reference = $dataBase->searchDB($reference_sql)[0];
                $reference = json_decode($data_reference['data'], true);
                $usuario['skills'] = $skills;
                $usuario['laboral_references'] = $laboral_references;
                $usuario['user_studies'] = $user_studies;
                $usuario['mastered_languaje'] = $mastered_languages;
                $usuario['futher_studie'] = $further_studie;
                $usuario['reference'] = $reference;
                echo json_encode($usuario, JSON_UNESCAPED_UNICODE);
                break;
            case 'actualizarDatos':
                $data = $_POST;
                echo "<pre>";
                print_r($_POST);
                $update_user_data = 'UPDATE user ' .
                    'SET name = "' . $data['name'] . '",' .
                    'last_name = "' . $data['last_name'] . '",' .
                    'email = "' . $data['email'] . '",' .
                    'phone = "' . $data['phone'] . '"' .
                    'WHERE cod_user = 1';
                $dataBase->executeDB($update_user_data);
                $sql_clear = 'DELETE FROM skills WHERE cod_user = 1';
                $dataBase->executeDB($sql_clear);
                $sql_clear = 'DELETE FROM laboral_references WHERE cod_user = 1';
                $dataBase->executeDB($sql_clear);
                $sql_clear = 'DELETE FROM mastered_languages WHERE cod_user = 1';
                $dataBase->executeDB($sql_clear);
                $sql_clear = 'DELETE FROM reference WHERE cod_user = 1';
                $dataBase->executeDB($sql_clear);
                $sql_clear = 'DELETE FROM user_studies WHERE cod_user = 1';
                $dataBase->executeDB($sql_clear);
                $skills = json_encode([
                    $data['skill'],
                    $data['skill2'],
                    $data['skill3'],
                    $data['sskill4'],
                    ['programas' => $data['Skillsprograms']],
                    ['nociones' => $data['Skillnotion']]
                ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
                $insert_skills = 'INSERT INTO skills (cod_user, data) VALUES (1, ' . $skills . ');';
                $dataBase->insertDB($insert_skills);
                $laboral_references = json_encode([
                    ['date' => [
                        $data['fechaInRef'],
                        $data['fechaFinRef'],
                        $data['ciudadRef'],
                    ]],
                    ['enterprise' => $data['empresa_ref']],
                    ['place' => $data['cargoRef']],
                    ['task' => $data['xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx']],
                    ['programas' => $data['Skillsprograms']],
                    ['nociones' => $data['Skillnotion']]
                ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
                $insert_laboral = 'INSERT INTO laboral_reference (cod_user, data) VALUES (1, ' . $laboral_references . ');';
                $dataBase->insertDB($insert_laboral);
                break;
            case 'eliminarDatos':
                $update_user_data = 'UPDATE user ' .
                    'SET name = "NN",' .
                    'last_name = "NN",' .
                    'email = "NOT EMAIL",' .
                    'phone = "NOT PHONE"' .
                    'WHERE cod_user = 1';
                $dataBase->executeDB($update_user_data);
                $sql_clear = 'DELETE FROM skills WHERE cod_user = 1';
                $dataBase->executeDB($sql_clear);
                $sql_clear = 'DELETE FROM laboral_references WHERE cod_user = 1';
                $dataBase->executeDB($sql_clear);
                $sql_clear = 'DELETE FROM mastered_languages WHERE cod_user = 1';
                $dataBase->executeDB($sql_clear);
                $sql_clear = 'DELETE FROM reference WHERE cod_user = 1';
                $dataBase->executeDB($sql_clear);
                $sql_clear = 'DELETE FROM user_studies WHERE cod_user = 1';
                $dataBase->executeDB($sql_clear);
                break;
            default:
                break;
        }
    }
}