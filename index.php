<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TALLER</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://kit.fontawesome.com/e180bdac5c.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="public/css/core.css">
    <script src="public/js/hdv.js"></script>
</head>

<body>
<br>
<br>
<section>
    <section class="left-col">
        <section class="name">
            <table class="complete-name">
                <tr>
                    <td class="zyan-name">
                        <b id="name1"> </b>
                    </td>
                    <td class="zyan-extension-name">
                        <p id="name"></p>
                    </td>
                </tr>
                <tr>
                    <td class="zyan-name">
                        <b id="last_name1"></b>
                    </td>
                    <td class="black-extension-name">
                        <p id="last_name"> </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
            <section class="gray">
                <span id="phone"> </span> | <span id="email"> </span>
            </section>
        </section>
        <article>
            <h1 class="green-title">
                <img src="public/images/menos.svg" alt="" height="3%" width="3%" id="iconSkill">
                HABILIDADES
            </h1>
        </article>
        <section class="skills" id="ocultarSkill">
            <section class="left-col col">
                <article id="skill1"></article>
                <article id="skill2"></article>
                <article id="skill3"></article>
            </section>
            <section class="right-col col">
                <article>
                    <p-zyan>
                        > Programas manejados:
                    </p-zyan>
                </article>
                <article>
                    <span id="programa1"></span><span id="programa2"></span><span id="programa3"></span>
                </article>
                <article>
                    <p-zyan>
                        > Nociones en:
                    </p-zyan>
                </article>
                <article>
                    <span id="nocion1"></span>
                    <span id="nocion2"></span>
                </article>
            </section>
        </section>
        <article>
            <h1 class="green-title">
                <img src="public/images/menos.svg" alt="" height="3%" width="3%" id="iconExperience">
                EXPERIENCIA LABORAL
            </h1>
        </article>
        <section class="laboral-experience" id="ocultarExperience">
            <section>
                <section class="gray col date-works">
                    <article id="date">

                    </article>

                    <article id="city">
                    </article>
                </section>
                <section class="experience">
                    <article class="gray" id="enterpise">

                    </article>
                    <article class="kyng-blue" id="place">

                    </article>
                    <article id="task">

                    </article>

                </section>

            </section>
        </section>
        <article>
            <h1 class="green-title">
                <img src="public/images/menos.svg" alt="" height="3%" width="3%" id="iconStudies">
                ESTUDIOS
            </h1>
        </article>
        <section class="study" id="ocultarStudies">
            <section class="circles">
                <img src="public/images/circles.png" alt="" width="28%">
            </section>
            <section class="experience right-col">
                <section>
                    <article class="gray">
                        <p id="year"></p> | <p id="city2"></p> <p id="country2"></p>
                    </article>
                    <article>
                        <p-zyan >
                            <p id="titulo"></p>
                        </p-zyan>
                    </article>
                    <article id="university">

                    </article>
                </section>

            </section>
        </section>
    </section>
    <section class="right-col">
        <section class="img-profile">

        </section>
        <section class="others">
            <section class="profile-picture">
                <img src="public/images/profile.png" alt="">
            </section>
            <section>
                <h1 class="titulo">
                    TITULO DEL TRABAJO
                </h1>
            </section>
            <div class="cajaTitulo" >
                <img src="public/images/minus.svg" alt="" height="8%" width="8%" id="iconIdiomas">
                IDIOMAS
            </div>
            <section class="languajes" id="ocultarIdiomas">
                <p > <span id="idioma1"></span> |<span id="nivel1"></span> </p>
                <p > <span id="idioma2"></span> |<span id="nivel2"></span> </p>
                <p > <span id="idioma3"></span> |<span id="nivel3"></span> </p>
                <br>
                <img src="public/images/world.png" alt="" class="world" height="100%">
            </section>
            <div class="cajaTitulo">
                <img src="public/images/minus.svg" alt="" height="8%" width="8%" id="iconFormacion">
                FORMACIONES ADICIONALES
            </div>
            <section class="aditional-study" id="ocultarFormacion">
                <p id="futher_studie"></p>
            </section>
            <div class="cajaTitulo">
                <img src="public/images/minus.svg" alt="" height="8%" width="8%" id="iconSocial">
                REDES SOCIALES
            </div>
            <section class="social-networks" id="ocultarSocial">
                <img src="public/images/social_networks.png" alt="" class="redes" height="150%">
            </section>
            <div class="cajaTitulo">
                <img src="public/images/minus.svg" alt="" height="8%" width="8%" id="iconReferencias">
                REFERENCIAS
            </div>
            <section class="references" id="ocultarReferencias">
                <div>
                        <span class="name">
                            <b id="nombreRef"> </b>
                        </span><br>
                    <span>
                            <p id="empresaref"></p>
                        </span><br>
                    <span>
                            <p id="cargoref"></p>
                        </span><br>
                    <span>
                            <p id="telfer"></p>
                        </span><br>
                    <span>
                           <p id="emailref"></p>
                        </span>
                </div><br>
                
            </section>
        </section>
    </section>
</section>
<div class="row">
    <div class="col-md-6">
        <button type="button" class="btn btn-primary editar">Editar Hoja de Vida</button>
    </div>
    <div class="col-md-6">
        <button type="button" class="btn btn-danger eliminar">Eliminar hoja de Vida</button>
    </div>
</div>
</body>

</html>