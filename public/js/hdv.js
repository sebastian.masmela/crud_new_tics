// IIFE - Immediately Invoked Function Expression
(function (yourcode) {

    // The global jQuery object is passed as a parameter
    yourcode(window.jQuery, window, document);

}(function ($, window, document) {

    $(function () {
        let iconSkill = document.getElementById('iconSkill')
        let iconExperience = document.getElementById('iconExperience')
        let iconStudies = document.getElementById('iconStudies')
        iconSkill.onclick = function ocultarInfo() {
            let section = document.getElementById("ocultarSkill")
            let iconImg = iconSkill.getAttribute('src')
            if (section.style.display === "none") {
                section.style.display = "block"
            } else {
                section.style.display = "none"
            }

            if (iconImg === '../../public/images/mas.svg') {
                iconSkill.setAttribute('src', '../../public/images/menos.svg')
            } else {
                iconSkill.setAttribute('src', '../../public/images/mas.svg')
            }
        }
        iconExperience.onclick = function ocultarInfo() {
            let section = document.getElementById("ocultarExperience")
            let iconImg = iconExperience.getAttribute('src')
            if (section.style.display === "none") {
                section.style.display = "block"
            } else {
                section.style.display = "none"
            }

            if (iconImg === '../assets/images/mas.svg') {
                iconExperience.setAttribute('src', '../../public/images/menos.svg')
            } else {
                iconExperience.setAttribute('src', '../../public/images/mas.svg')
            }
        }
        iconStudies.onclick = function ocultarInfo() {
            let section = document.getElementById("iconStudies")
            let iconImg = iconStudies.getAttribute('src')
            if (section.style.display === "none") {
                section.style.display = "block"
            } else {
                section.style.display = "none"
            }

            if (iconImg === '../../public/images/mas.svg') {
                iconStudies.setAttribute('src', '../../public/images/menos.svg')
            } else {
                iconStudies.setAttribute('src', '../../public/images/mas.svg')
            }
        }
        traerDatosHv()

        $('.editar').click(function () {
            window.location.href = 'http://localhost/crud_new_tics/app/views/form.html'
        })
    });

    function traerDatosHv() {
        $.ajax({
            cache: false,
            async: true,
            url: "http://localhost/crud_new_tics/app/controllers/API.php?funcion=obtenerUsuario&cod_usuario=1",
            method: 'GET',
            dataType: 'json',
            success: function(data) {

                console.log(data)

                let $nombre = $('#name')
                $nombre.append('<p>'+ data.name +'</p>')

                let $apellido = $('#last_name')
                $apellido.append('<p>'+ data.last_name +'</p>')

                let $telefono = $('#phone')
                $telefono.append('<p>'+ data.phone +'</p>')

                let $email= $('#email')
                $email.append('<p>'+data.email+'</p>')

                let $hablidad = $('#skill')
                $hablidad.append('<p>'+data.skill+'</p>')

                let $redesSociales = $('#redesSociales')
                $redesSociales.append('<p>redesSociales</p>')

                let $estudio = $('#estudio')
                $estudio.append('<p>estudio</p>>')
            }
        })
    }
}))